from pymongo import MongoClient
from nameDB import names
import datetime
# setting up the connection to the database


# creating the class
class future_product_db():
    '''
    connections to the mongo db database
    scraper is the database
    products is a collection with in the database
    '''
    client = MongoClient('mongodb+srv://not_the_app:MTAfkyXDOZJi8t2A@proto.d0ftd.mongodb.net/scraper?retryWrites=true&w=majority')
    collection = client.scraper.products
    '''keys for dict generation'''
    type_key = 'type'
    name_key = "name"
    category_key = "category"
    price_key = "price"
    description_key = "description"
    scrape_key = "scrape_time"
    release_key = "release_datetime"
    url_key = "url_extension"



    def __init__(self,name,description,category,price,url,scrape ,release):
        self.name = name
        self.description = description
        self.category = category
        self.price = price
        self.url = url
        self.scrape = scrape 
        self.type = "current"


    '''generates a  dict object based on the values held the object'''
    def convert_to_dict(self):
        return  {
            self.name_key:self.name,
            self.category_key:self.category,
            self.description_key:self.description,
            self.price_key : self.price,
            self.url_key:self.url,
            self.scrape_key:self.scrape
        }


    ''' 
        checks for any old objects with the same name and deletes them
        then generates the current object and uploads it to the database
    '''
    def update(self):
        if names.collection.count()>0:
            old_n = names.collection.find_one_and_delete({self.name_key:self.name})
            old_p = collection.find_one_and_delete({names.id_key:old_n['id']})

        p = self.convert_to_dict()
        id = str(collection.find_one({self.name_key:p[self.name_key]})['_id'])
        names(self.name,id).update()
        return id
    
class NoProductError(Exception):
    pass

def find_product_by_name(name:str):
    if type(name) == 'str':
        raise Exception("input is invalid type")

